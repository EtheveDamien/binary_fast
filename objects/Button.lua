local FM = require("tools/FontManager")
local CM = require("tools/CollisionManager")

--[[
  isOutline       : true / false use to draw outline
  isBackground    : true / false use to draw background or not
  outlineColor    : { r, v ,b , a }
  backgroundColor : { r, v ,b , a }
  textColor       : { r, v ,b , a }
  text            : text that will be print
  fn              : code that will be execute
  x, y            : positions
  width, height   : dimensions
]]

return {
  new = function (text,fn,x,y,w,h)
    local obj = {}
    obj.x = x or 0
    obj.y = y or 0
    obj.width = w or 5
    obj.height = h or 5
    obj.text = text or "empty_button"
    obj.fn = fn or function() print("empty") end
    obj.textColor = {0, 0, 0, 1}
    obj.isBackground = false
    obj.backgroundColor = {1, 1, 0, 0.5}
    obj.outlineColor = {0, 0, 0, 1}
    obj.isOutline = true
    obj.now = false
    obj.last = false

    function obj:update(dt)

    end

    function obj:draw()
      -- draw background
      if self.isBackground then
        love.graphics.setColor(unpack(self.backgroundColor))
        love.graphics.rectangle(
          "fill",
          self.x,
          self.y,
          self.width,
          self.height
        )
      end

      -- draw outline
      if self.isOutline then
        love.graphics.setColor(unpack(self.outlineColor))
        love.graphics.rectangle(
          "line",
          self.x,
          self.y,
          self.width,
          self.height
        )
      end

      -- draw text
      love.graphics.setColor(unpack(self.textColor))
      local tw = FM.small:getWidth(self.text)
      local th = FM.small:getHeight(self.text)
      love.graphics.print(
        self.text,
        FM.small,
        self.x + self.width/2 - tw/2,
        self.y + self.height/2 - th/2
      )
    end

    function obj:mousereleased(x, y, button, istouch, presses)
      local hot = CM.pointInBox(
        x, y,
        self.x, self.y,
        self.width, self.height
      )
      if hot and button == 1 then
        self.fn()
      end
    end

    return obj
  end
}

local FM = require("tools/FontManager")
local CM = require("tools/CollisionManager")
local Button = require("objects/Button")
local Settings = require("scenes/Settings")

--[[
  prerequisites : WIDTH, HEIGHT, SCORE, BEST_SCORE
]]

return {
  new = function ()
    local obj = {}
    obj.height = (1/25) * HEIGHT
    obj.button = nil
    obj.score = SCORE
    obj.bestScore = BEST_SCORE

    function obj:load()
      self.button = Button.new(
        "Settings",
        function ()
          PAUSE = not PAUSE
          Settings:load()
        end,
        0, 0,
        (1/3)*WIDTH, self.height
      )
    end

    function obj:update(dt)
      if SCORE > BEST_SCORE then BEST_SCORE = SCORE end
      self.score = SCORE
      self.bestScore = BEST_SCORE
    end

    function obj:draw()
      love.graphics.setColor(0.97,0.97,0.97)
      love.graphics.rectangle("fill", 0,0, WIDTH, self.height)
      love.graphics.setColor(0,0,0)
      love.graphics.rectangle(
        "line", 
        (2/3) * WIDTH - WIDTH/3,
        0, 
        WIDTH/3, self.height
      )

      love.graphics.rectangle(
        "line", 
        (3/3) * WIDTH - WIDTH/3,
        0, 
        WIDTH/3, self.height
      )
      love.graphics.setColor(1,1,1)

      self.button:draw()

      local scoreW = FM.small:getWidth(self.score)
      local scoreH = FM.small:getHeight(self.score)
      love.graphics.print(
        self.score,
        FM.small,
        (2/3) * WIDTH - WIDTH/3/2 - scoreW/2,
        self.height/2 - scoreH/2
      )

      local bestScoreW = FM.small:getWidth(self.bestScore)
      local bestScoreH = FM.small:getHeight(self.bestScore)
      love.graphics.print(
        self.bestScore,
        FM.small,
        (3/3) * WIDTH - WIDTH/3/2 - bestScoreW/2,
        self.height/2 - bestScoreH/2
      )
    end

    function obj:mousereleased(x, y, button, istouch, presses)
      self.button:mousereleased(x, y, button, istouch, presses)
    end

    return obj
  end
}

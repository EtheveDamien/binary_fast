local FM = require("tools/FontManager")
local CM = require("tools/CollisionManager")

local Button = require("objects/Button")
local HeaderBar = require("objects/HeaderBar")
local ProgressBar = require("objects/ProgressBar")
local Settings = require("scenes/Settings")
local Loose = require("scenes/Loose")

local Game = {}
local initialTime = 5

function Game:load()
  -- init Game variables
  CODE_LENGHT = 4
  SCORE = 0
  Game.buttons = {}
  Game.time = initialTime
  Game.actualCode = RB.randomCode(CODE_LENGHT)
  Game.actualResponse = RB.calculResponse(Game.actualCode)
  Game.responseCode = ""
  Game.header = HeaderBar.new()
  Game.timeBar = ProgressBar.new(0, initialTime, initialTime)

  -- init header
  self.header:load()

  -- init loose screen
  Loose:load()

  -- create buttons
  table.insert( self.buttons, Button.new(
    "0",
    function ()
      self.responseCode = self.responseCode.."0"
    end,
    WIDTH/2 - 2* WIDTH/5, HEIGHT - HEIGHT/7,
    WIDTH/5, WIDTH/5
  ))

  table.insert( self.buttons, Button.new(
    "1",
    function ()
      self.responseCode = self.responseCode.."1"
    end,
    WIDTH/2 + 2* WIDTH/5 - WIDTH/5, HEIGHT - HEIGHT/7,
    WIDTH/5, WIDTH/5
  ))
end

function Game:update(dt)
  -- update header
  self.header:update(dt)
  self.timeBar:update(dt)

  -- update logic
  if PAUSE == false and LOOSE == false then
     -- decrease time
    self.time = self.time - dt
    self.timeBar.value = self.time
    if self.time <= 0 then
      LOOSE = true
      self:_reset()
    end

    if RB.isEqual(self.responseCode, self.actualResponse) == false then
      LOOSE = true
      self:_reset()
    end

    if string.len(self.responseCode) == string.len(self.actualCode) then
      if self.responseCode == self.actualResponse then
        SCORE = SCORE + 1
        self:_reset() 
      elseif self.responseCode ~= self.actualResponse then
        LOOSE = true
        self:_reset()
      end
    end
  end
end

function Game:draw()
  -- draw buttons
  for i,v in ipairs(self.buttons) do
    v:draw()
  end

  -- draw header
  self.header:draw()

  -- draw actualcode
  local act = tostring(self.actualCode)
  local atw = FM.code:getWidth(act)
  local ath = FM.code:getHeight(act)
  love.graphics.print(
    act,
    FM.code,
    WIDTH/2 - atw/2,
    HEIGHT/3 - ath/2
  )

  love.graphics.line(0, HEIGHT/2, WIDTH, HEIGHT/2)

  -- draw user response
  local rep = tostring(self.responseCode)
  local tw = FM.code:getWidth(rep)
  local th = FM.code:getHeight(rep)
  love.graphics.print(
    rep,
    FM.code,
    WIDTH/2 - tw/2,
    HEIGHT - HEIGHT/3 - th/2
  )

  -- draw timebar
  self.timeBar:draw()

  if PAUSE == true and LOOSE == false then
    Settings:draw()
    love.graphics.setColor(1,1,1,1)
    local pauseText = "PAUSE"
    local tw = FM.main:getWidth(pauseText)
    local th = FM.main:getHeight(pauseText)
    love.graphics.print(
      pauseText,
      FM.main,
      WIDTH/2 - tw/2,
      HEIGHT/4 - th/2
    )
  end

  if LOOSE == true then
    PAUSE = true
    Loose:draw()

    local tsw = FM.title:getWidth(SCORE)
    local tsh = FM.title:getHeight(SCORE)
    love.graphics.print(
      SCORE,
      FM.title,
      WIDTH/2 - tsw/2,
      HEIGHT/4 - tsh/2 
    )
  end
end

function Game:mousereleased(x, y, button, istouch, presses)
  self.header:mousereleased(x, y, button, istouch, presses)
  if not PAUSE and LOOSE == false then
    for i,v in ipairs(self.buttons) do
      v:mousereleased(x, y, button, istouch, presses)
    end
  end
  if PAUSE == true and LOOSE == false then
    Settings:mousereleased(x, y, button, istouch, presses)
  end
  if LOOSE == true then
    Loose:mousereleased(x, y, button, istouch, presses)
  end
end

-- private function
function Game:_reset()
  -- update level
  if SCORE >= 5 and SCORE < 10 then
    self.timeBar:redimention(5)
    self.time = 5
    CODE_LENGHT = 5
  elseif SCORE >= 10 and SCORE < 15 then
    self.timeBar:redimention(5)
    self.time = 5
    CODE_LENGHT = 6
  elseif SCORE >= 15 and SCORE < 20 then
    self.timeBar:redimention(4)
    self.time = 4
    CODE_LENGHT = 7
  elseif SCORE >= 20 then
    self.timeBar:redimention(3)
    self.time = 3
    CODE_LENGHT = 8
  else
    self.time = initialTime
  end

  self.actualCode = RB.randomCode(CODE_LENGHT)
  self.actualResponse = RB.calculResponse(self.actualCode)
  self.responseCode = ""
end

return Game

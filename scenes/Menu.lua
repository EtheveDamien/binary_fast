local FM = require("tools/FontManager")
local CM = require("tools/CollisionManager")
local Button = require("objects/Button")

local Menu = {}


function Menu:load()
  Menu.buttons = {}
  table.insert(self.buttons, Button.new(
    "Start Game",
    function ()
      SM:setScene("Game")
    end
  ))

  table.insert(self.buttons, Button.new(
    "Settings",
    function ()
      SM:setScene("Settings")
    end
  ))

  table.insert(self.buttons, Button.new(
    "About",
    function ()
      SM:setScene("About")
    end
  ))

  table.insert(self.buttons, Button.new(
    "Exit",
    function ()
      love.event.quit(0)
    end
  ))
end

function Menu:update(dt)

end

function Menu:draw()
  local margin = 16
  local cursor_y = 0

  for i,v in ipairs(self.buttons) do
    v.width = WIDTH/2.5
    v.height = 60
    local total_height = (v.height + margin) * #self.buttons
    v.x = WIDTH/2 - v.width/2
    v.y = HEIGHT/2 - total_height/2 + cursor_y
    cursor_y = cursor_y + (v.height + margin)
    v:draw()
  end

  local tw = FM.title:getWidth(GAME_NAME)
  local th = FM.title:getHeight(GAME_NAME)
  love.graphics.print(
    GAME_NAME,
    FM.title,
    WIDTH/2 - tw/2,
    HEIGHT/4 - th/2
  )
end

function Menu:mousereleased(x, y, button, istouch, presses)
  for i,v in ipairs(self.buttons) do
    v:mousereleased(x, y, button, istouch, presses)
  end
end

return Menu

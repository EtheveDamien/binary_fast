-- global variables
GAME_NAME       = "Binary Fast"
SCORE           = 0
BEST_SCORE      = 0
CODE_LENGHT     = 4

WIDTH           = love.graphics.getWidth()
HEIGHT          = love.graphics.getHeight()
SW              = (WIDTH / 540)
SH              = (HEIGHT / 960)
PAUSE           = false
LOOSE           = false

-- global accessors
RB = require("tools/RandomBinary")
SM = require("tools/SceneManager")

-- local accessors
local saveFile = "score"
local saveFileName = saveFile..".save"

function love.load()
  if love.filesystem.getInfo(saveFileName) ~= nil then
    local content, size = love.filesystem.read(saveFileName)
    BEST_SCORE = tonumber(content)
  end

  love.graphics.setBackgroundColor(1,1,1)
  SM:load() -- load Menu by default
end

function love.update(dt)
  SM:update(dt)
end

function love.draw()
  SM:draw()
end

function love.mousereleased(x, y, button, istouch, presses)
  SM:mousereleased(x, y, button, istouch, presses)
end

function love.keypressed(key)
  if key == "escape" then
     love.event.quit()
  end
end

function love.quit()
  local success, message = love.filesystem.write(saveFileName, tostring(BEST_SCORE))
end
